from threading import Thread
import time

import RPi.GPIO as GPIO

import board
import adafruit_dht 

class Sensors(Thread):
    def __init__(self, sensors_state, sql_helper):

        Thread.__init__(self)
        self.sensors_state = sensors_state
        self.smoke_value = False
        self.vib_value = False
        self.last_temp = 0
        self.last_hum = 0

        self.sql_helper = sql_helper

        #DOOR SENSOR INIT
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(12,GPIO.OUT)
        
        #SMOKE SENSOR INIT
        GPIO.setup(19,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        def smoke_callback(pin):
            self.smoke_value = True
            return
        GPIO.add_event_detect(19, GPIO.RISING)
        GPIO.add_event_callback(19, smoke_callback)

        #TEMP & HUMIDITY INIT
        self.dhtDevice = adafruit_dht.DHT22(board.D17)

        #VIBRATION INIT
        channel = 27
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(channel, GPIO.IN)
        def vib_callback(channel):
                if GPIO.input(channel):
                    self.vib_value = True

        GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=300)
        GPIO.add_event_callback(channel, vib_callback)

        #MOTION INIT
        self.GPIO_PIR = 4
        GPIO.setup(self.GPIO_PIR,GPIO.IN)

    def change_thread_state(self,state):
        self.sensors_state=state
       
    def run(self):

        while True:
            sensor_data = dict()
            if self.sensors_state["door"]:
                if GPIO.input(14):
                    sensor_data["door"] = False
                    GPIO.output(12,GPIO.LOW)
                else:
                    sensor_data["door"] = True
                    GPIO.output(12,GPIO.HIGH)
            else:
                sensor_data["door"]=None

            if self.sensors_state["smoke"]:
                if self.smoke_value:
                    sensor_data["smoke"] = True
                    self.smoke_value = False
                else:
                    sensor_data["smoke"] = False
            else:
                sensor_data["smoke"] = None
            

            try:
                if self.sensors_state["temperature"]:
                    if self.dhtDevice.temperature:
                        sensor_data["temp"] = self.dhtDevice.temperature
                    else:
                        sensor_data["temp"] = self.last_temp

                    if self.dhtDevice.temperature:
                        self.last_temp = self.dhtDevice.temperature
                else:
                    sensor_data["temp"] = None
                    self.last_temp = None

                if self.sensors_state["humidity"]:
                    if self.dhtDevice.humidity:
                        sensor_data["hum"] = self.dhtDevice.humidity
                    else:
                        sensor_data["hum"] = self.last_hum

                    if self.dhtDevice.humidity:
                        self.last_hum = self.dhtDevice.humidity
                else:
                    sensor_data["hum"] = None
                    self.last_hum = None

            except RuntimeError as error:
                sensor_data["temp"] = self.last_temp
                sensor_data["hum"] = self.last_hum
                continue

            except Exception as error:
                #dhtDevice.exit()
                raise error

                
            if self.sensors_state["vibration"]:
                if self.vib_value:
                    sensor_data["vib"] = True
                    self.vib_value = False
                else:
                    sensor_data["vib"] = False
            else:
                sensor_data["vib"] = None

            
            if self.sensors_state["motion"]:
                if GPIO.input(self.GPIO_PIR):
                    sensor_data["motion"] = True
                else:
                    sensor_data["motion"] = False
            else:
                sensor_data["motion"] = None


            self.sql_helper.insert_reading(sensor_data)
            
            #print(sensor_data["door"])
            #print(sensor_data["smoke"])
            #print(str(sensor_data["temp"]) + " " + str(sensor_data["hum"]))
            #print(sensor_data["vib"])
            #print(sensor_data["motion"])

            # print("D "+str(sensor_data["door"])+" "+
            # "S "+str(sensor_data["smoke"])+" "+
            # "T "+str(sensor_data["temp"])+" "+
            # "H "+str(sensor_data["hum"])+" "+
            # "V "+str(sensor_data["vib"])+" "+
            # "M "+str(sensor_data["motion"])+" ")
            time.sleep(10)
        
        GPIO.cleanup()
