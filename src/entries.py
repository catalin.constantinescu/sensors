from threading import Thread
import time
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522


class RoomEntries(Thread):

    def __init__(self,sql_helper):

        Thread.__init__(self)
        self.reader = SimpleMFRC522()
        self.sql_helper = sql_helper
       
    def run(self):

        while True:
            try:
                id = self.reader.read()
                self.sql_helper.insert_entry(id[0])
                time.sleep(5)
            finally:
                print()


