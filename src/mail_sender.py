from threading import Thread
import smtplib
import time
from helpers.singleton import Singleton
from queue import Queue

class MailSender(Thread):
    __metaclass__ = Singleton

    def __init__(self,mail_url,port,credentials, target):

        Thread.__init__(self)

        self.server = smtplib.SMTP(mail_url,port)
        self.credentials = credentials
        self.target = target
        self.mail_queue = Queue()
        self.server.starttls()
        self.server.login(self.credentials["user"], self.credentials["password"])

    def send_message(self, msg):
        self.server.sendmail(self.credentials["user"],self.target,msg)

    def send_email(self,msg):
        self.mail_queue.put(msg)

    def run(self):
        while True:
            if not self.mail_queue.empty():
                self.send_message(self.mail_queue.get())
            time.sleep(1)

