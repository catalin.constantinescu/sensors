import time
import mysql.connector
from helpers.singleton import Singleton

class SqlHelper:    
    __metaclass__ = Singleton

    def __init__(self,mail_sender):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="sensor_user",
            password="1234",
            database="sensors"
        )
        self.mail_sender = mail_sender

    def insert_entry(self, tag_id): #metoda a clasei
        mycursor = self.mydb.cursor(buffered=True)
        select_query = "SELECT id FROM room_user WHERE tag_id=%s"
        mycursor.execute(select_query, (tag_id,))
        user_id = mycursor.fetchone()

        if not user_id:
            self.mail_sender.send_email("Someone with an unregistered tag (" + str(tag_id) + ") tried to enter the room!")
        else:
            insert_query = "INSERT INTO entry " \
                        "(user_id, timestamp) " \
                        "VALUES (%s, %s)"
            val = (user_id[0], int(time.time()))
            mycursor.execute(insert_query, val)

            self.mydb.commit()
            mycursor.close()

    def insert_reading(self, sensor_data):
        mycursor = self.mydb.cursor()
        insert_query = "INSERT INTO reading " \
                       "(temperature, humidity, motion, door, smoke, vibration, timestamp) " \
                       "VALUES (%s, %s, %s, %s, %s, %s, %s)"
        val = (sensor_data["temp"], 
        sensor_data["hum"], 
        sensor_data["motion"], 
        sensor_data["door"], 
        sensor_data["smoke"], 
        sensor_data["vib"], 
        int(time.time()))

        mycursor.execute(insert_query, val)
        self.mydb.commit()
        mycursor.close()

    def close_connection(self):
        self.mydb.close()
