from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name='sensors_project',
    version='1.0.0',
    description='Datacenter Sensor Project',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='',
    author='Dirdala C.',
    author_email='cipriandtest@gmail.com',
    keywords='sample, setuptools, development',
    packages='src',
    python_requires='>=3.6, <4',
    install_requires=[],
    extras_require={  # Optional
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
    package_data={},  # Optional
    data_files=[],  # Optional
    entry_points={  # Optional
    }
)