
import logging
import logging.handlers
import argparse

class Logger(object):
        def __init__(self, level, filename=None):
                LOG_FILENAME = "/tmp/sensor_service.log"
                LOG_LEVEL = logging.INFO

                # If the log file is specified on the command line then override the default
                if filename!=None:
                    LOG_FILENAME = filename

                # Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
                # Give the logger a unique name (good practice)
                logger = logging.getLogger(__name__)
                # Set the log level to LOG_LEVEL
                logger.setLevel(LOG_LEVEL)
                # Make a handler that writes to a file, making a new file at midnight and keeping 3 backups
                handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
                # Format each log message like this
                formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
                # Attach the formatter to the handler
                handler.setFormatter(formatter)
                # Attach the handler to the logger
                logger.addHandler(handler)
                """Needs a logger and a logger level."""
                self.logger = logger
                self.level = level

        def write(self, message, level=None):
                # Only log if there is a message (not just a new line)
                if message.rstrip() != "":
                        self.logger.log(self.level if level==None else level, message.rstrip())


